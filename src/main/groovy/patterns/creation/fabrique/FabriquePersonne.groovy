package patterns.creation.fabrique

final class FabriquePersonne {

    static Personne creer(String nom, String type) {
        new PersonneMorale(nom, type as TypePersonneMorale)
    }

    static Personne creer(String nomFamille,
                          String prenom,
                          String dateNaissance) {
        Date date = Date.parse("yyyy-MM-dd", dateNaissance)
        new PersonnePhysique(nomFamille, prenom, date)
    }

    static Personne creer(Map args) {
        if (args.nom && args.type) {
            creer(args.nom, args.type)
        } else if (args.nomFamille && args.prenom && args.dateNaissance) {
            creer(args.nomFamille, args.prenom, args.dateNaissance)
        }
    }

}
