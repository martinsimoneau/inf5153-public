package patterns.creation.fabrique

class PersonneMorale extends Personne {

    TypePersonneMorale type
    String nom

    PersonneMorale(String nom, TypePersonneMorale type) {
        this.nom = nom
        this.type = type
    }

    String toString() { "${super.toString()}Type : $type\n\t" }

}
